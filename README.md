# Leekboard [![build status](https://gitlab.com/llinksrechts/leekboard/badges/master/build.svg)](https://gitlab.com/llinksrechts/leekboard/commits/master) [![leekboard.lukor.org](https://img.shields.io/website-up-down-green-red/https/leekboard.lukor.org.svg?label=leekboard.lukor.org)](https://leekboard.lukor.org/)
Online synthesizing software for button pushers. Latest version accessible at [https://leekboard.lukor.org/](https://leekboard.lukor.org/).

## Usage
### Installation
Simply clone into a directory and point an Apache vhost to this directory. To
use Google Drive and Dropbox integration, an API key has to be requested and
inserted into the respective JavaScript file (`scripts/dropbox.js` and `scripts/gdrive.js`).

### User interface
How to use the interface is explained in [/guide/](https://leekboard.lukor.org/guide/).

## Contributing
For a guide on how to contribute to this project, see [CONTRIBUTING.md](https://gitlab.com/llinksrechts/leekboard/blob/master/CONTRIBUTING.md).

## Changelog
The changelog can be found in [CHANGELOG.md](https://gitlab.com/llinksrechts/leekboard/blob/master/CHANGELOG.md).

## License
This project is available under the [Apache License 2.0](https://gitlab.com/llinksrechts/leekboard/blob/master/LICENSE).

## Authors
* [Lukas Rysavy](https://home.lukor.org/)

## Contributors
* None
