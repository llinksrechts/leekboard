var editorScale;
var curSeq;
var selectedEvents = [];
var cancelDragging = false;
var isDragging = false;
var sequenceRecording = false;
var newEvents = [];
var startEventRecord;

function editSequences() {
    $(".frontWrapper").hide();
    $("#sequenceWrapper").show();
    mode = "sequenceEditor";
    reloadSequenceList();
}

function editThisSequence() {
    var seqVal = $("#buttonSequence").val();
    hideSettings();
    editSequences();
    $("#sequenceList").val(seqVal);
    selectSequence();
}

function createSequence() {
    var name;
    do {
        name = prompt("Enter a name for your new sequence", "New sequence");
        if(name === null) {
            return;
        }
        if(sequences[name] !== undefined) {
            alert("A sequence with that name already exists. Please enter a different name.");
        }
    } while(sequences[name] !== undefined);
    sequences[name] = [];
    curSeq = name;
    reloadSequenceList();
    showSequenceEditor();
    $("#sequenceList").val(name);
    saveSequences();
    return name;
}

function cloneSequence() {
    var source = $("#sequenceList").val();
    if(source === null) {
        return;
    }
    var dest = createSequence();
    if(dest === undefined) {
        return;
    }
    sequences[dest] = JSON.parse(JSON.stringify(sequences[source]));
    redrawTimeline();
    saveSequences();
}

function deleteSequence() {
    var name = $("#sequenceList").val();
    if(confirm("Do you really want to delete the sequence '" + name + "'?")) {
        delete sequences[name];
        reloadSequenceList();
        saveSequences();
    }
}

function reloadSequenceList() {
    $(".sequenceList").children().remove();
    for(var seq in sequences) {
        $(".sequenceList").append($("<option>").val(seq).html(seq));
    }
    hideSequenceEditor();
}

function saveSequences() {
    localStorage.setItem("sequences", JSON.stringify(sequences));
}

function loadSequences() {
    sequences = JSON.parse(localStorage.getItem("sequences"));
    if(sequences === null) {
        sequences = {};
    }
    snapSettingChange();
}

function selectSequence() {
    selectedEvents = [];
    curSeq = $("#sequenceList").val();
    if(curSeq === null) {
        hideSequenceEditor();
    } else {
        showSequenceEditor();
    }
}

function hideSequenceEditor() {
    $("#noSequenceSelected").show();
    $("#selectedSequence").hide();
}

function showSequenceEditor() {
    $("#noSequenceSelected").hide();
    $("#selectedSequence").show();
    redrawTimeline();
}

$("body").ready(loadSequences);

function stopModify(event, ui) {
    isDragging = false;
    if(!cancelDragging) {
        var parentBoundingRect = event.target.parentNode.getBoundingClientRect();
        var full = parentBoundingRect.right - parentBoundingRect.left;

        var posMove = ui.position.left - ui.originalPosition.left;
        var sizeChange = 0;
        if(ui.size !== undefined) {
            sizeChange = ui.size.width - ui.originalSize.width;
        }
        var leftPercent = posMove / full;
        var rightPercent = sizeChange / full;

        var thisId = parseInt(event.target.id.substr(3));
        var prevStart = sequences[curSeq][thisId].start;
        sequences[curSeq][thisId].start = Math.max(sequences[curSeq][thisId].start + leftPercent * editorScale, 0);
        sequences[curSeq][thisId].end += rightPercent * editorScale + sequences[curSeq][thisId].start - prevStart;
        if(selectedEvents.indexOf(thisId) != -1) {
            selectedEvents.forEach(function(event) {
                if(event == thisId) {
                    return;
                }
                var barRect = $("#bar" + event)[0].getBoundingClientRect();
                leftPercent = posMove / full;
                rightPercent = sizeChange / full;
                prevStart = sequences[curSeq][event].start;
                sequences[curSeq][event].start = Math.max(sequences[curSeq][event].start + leftPercent * editorScale, 0);
                sequences[curSeq][event].end += rightPercent * editorScale + sequences[curSeq][event].start - prevStart;
            });
        }
    }
    cancelDragging = false;
    redrawTimeline();
    if(selectedEvents.length == 1 && selectedEvents[0] == event.target.id.substr(3)) {
        showDigitalControls();
    }
}

function stateChange(event, ui) {
    if(cancelDragging) {
        event.preventDefault();
    }
    isDragging = true;
    var thisId = parseInt(event.target.id.substr(3));
    if(selectedEvents.indexOf(thisId) != -1) {
        var posMove = ui.position.left - ui.originalPosition.left;
        var sizeChange = 0;
        var barContainer = $(".timelineElement > td.noPadding");
        if(ui.size !== undefined) {
            sizeChange = ui.size.width - ui.originalSize.width;
        }
        selectedEvents.forEach(function(event) {
            if(event == thisId) {
                return;
            }
            var bar = sequences[curSeq][event];
            var toEdit = $("#bar" + event);
            var fullWidth = barContainer.width();
            var startPercent = bar.start / editorScale;
            var endPercent = bar.end / editorScale;
            var finalLeft = Math.min(Math.max(fullWidth * startPercent + posMove, 0), fullWidth);
            toEdit.css("left", finalLeft + "px");
            toEdit.css("width", Math.min(fullWidth * (endPercent - startPercent) + sizeChange, fullWidth - finalLeft) + "px");
        });
    }
}

function selectBar(event) {
    if(!event.target.id.startsWith("bar")) {
        return;
    }
    var id = parseInt(event.target.id.substr(3));
    if(!event.shiftKey && (selectedEvents.length > 1 || selectedEvents[0] != id)) {
        selectedEvents.forEach(function(id) {
            $("#bar" + id).removeClass("selectedBar");
        });
        selectedEvents = [];
    }
    if(selectedEvents.indexOf(id) < 0) {
        $(event.target).addClass("selectedBar");
        selectedEvents.push(id);
    } else {
        $(event.target).removeClass("selectedBar");
        selectedEvents.splice(selectedEvents.indexOf(id), 1);
    }
    if(selectedEvents.length == 1) {
        showDigitalControls();
    } else {
        hideDigitalControls();
    }
}
function selectGroup(event) {
    var key = parseInt(event.target.id.substr(8));
    var newSelected = sequences[curSeq].map(
        function(element, id) {
            if(element.char == key) { return id; }
        }
    ).filter(
        function(element){
            if(element!==undefined) { return true; }
        });
    var deselect = $(newSelected).not(selectedEvents).get().length == 0 &&
        ($(selectedEvents).not(newSelected).get().length == 0 || event.shiftKey);
    if(!event.shiftKey) {
        selectedEvents.forEach(function(id) {
            $("#bar" + id).removeClass("selectedBar");
        });
        selectedEvents = [];
    }
    if(deselect) {
        newSelected.forEach(function(id) {
            $("#bar" + id).removeClass("selectedBar");
            selectedEvents.splice(selectedEvents.indexOf(id), 1);
        });
    } else {
        newSelected.forEach(function(id) {
            if(selectedEvents.indexOf(id) < 0) {
                $("#bar" + id).addClass("selectedBar");
                selectedEvents.push(id);
            }
        });
    }
}

function showDigitalControls() {
    $("#digitalEventInput").show();
    $("#digitalEventStart").val(sequences[curSeq][selectedEvents[0]].start.toPrecision(2));
    $("#digitalEventEnd").val(sequences[curSeq][selectedEvents[0]].end.toPrecision(2));
}
function hideDigitalControls() {
    $("#digitalEventInput").hide();
}

function digitalEventChange() {
    sequences[curSeq][selectedEvents[0]].start = parseFloat($("#digitalEventStart").val());
    sequences[curSeq][selectedEvents[0]].end = parseFloat($("#digitalEventEnd").val());
    redrawTimeline();
}

function drawTimeline(maxVal) {
    var stepSize = roundToLeftmost(maxVal / 10);
    maxVal = Math.ceil(maxVal / stepSize) * stepSize;
    editorScale = maxVal;
    $("#lastTimeline").html(maxVal);
    $("#sequenceTimeline").children().remove();
    for(var i = 0; i < maxVal; i += stepSize) {
        $("#sequenceTimeline").append($("<td>").html(i));
    }
}

function redrawTimeline() {
    if(sequences[curSeq] === undefined) { return; }
    saveSequences();
    var prevEditorScale = editorScale;
    var maxVal = Math.max(Math.max.apply(Math, sequences[curSeq].map(function(element) { return element.end; })) + 0.1, 10);
    if(selectedEvents.length != 1) {
        hideDigitalControls();
    }
    drawTimeline(maxVal);
    redrawBars();
}

function redrawBars() {
    $(".timelineElement").remove();
    if(sequences[curSeq].length == 0) {
        $("#timelineBody").append($("<tr>").addClass("timelineElement").append($("<td>")).append($("<td>").html("No events yet")));
    }

    var keys = {};
    sequences[curSeq].forEach(function(element, index) {
        element.index = index;
        var list = keys[element.char];

        if(list) {
            list.push(element);
        } else {
            keys[element.char] = [element];
        }
    });
    var rows = {};
    var keySortFunction = function(event) {
        var hasSlot = false;
        for(var i = 0; i < rows[key].length; i++) {
            if(event.start > rows[key][i][rows[key][i].length-1].end) {
                rows[key][i].push(event);
                hasSlot = true;
                break;
            }
        }
        if(!hasSlot) {
            rows[key].push([event]);
        }
    };
    for(var key in keys) {
        keys[key].sort(function(a, b) {
            return a.start - b.start;
        });
        rows[key] = [];
        keys[key].forEach(keySortFunction);
    }
    var processKeyRows = function(row, index) {
        row.forEach(function(bar) {
            var newBar = $("<div>").addClass("timelineBar clickable").attr("id", "bar" + bar.index).html("&nbsp;")
                .css("top", index / element.length * 100 + "%");
            addBarEvents(newBar);
            barContainer.append(newBar);

            var fullWidth = barContainer.width();
            var startPercent = bar.start / editorScale;
            var endPercent = bar.end / editorScale;
            newBar.css("left", fullWidth * startPercent + "px");
            newBar.css("width", fullWidth * (endPercent - startPercent) + "px");
            newBar.css("position", "absolute");
        });
    };
    for(key in rows) {
        var element = rows[key];
        var newRow = $("<tr>").addClass("timelineElement").attr("id", "timeline" + key)
            .append($("<td>").html(getButtonName(key)).addClass("clickable").attr("id", "barGroup" + key).click(selectGroup));
        var barContainer = $("<td>").addClass("noPadding");
        newRow.append(barContainer);
        $("#timelineBody").append(newRow);

        element.forEach(processKeyRows);

        newRow.height(newRow.height() * element.length);
    }
    selectedEvents.forEach(function(id) {
        $("#bar" + id).addClass("selectedBar");
    });
    addSnap();
}

function addBarEvents(newBar) {
    newBar.draggable({
        containment: 'parent',
        axis: 'x',
        stop: stopModify,
        drag: stateChange,
    }).resizable({
        handles: 'e,w',
        containment: 'parent',
        stop: stopModify,
        resize: stateChange,
    }).click(selectBar);
}

function createTlEvent() {
    $(".frontWrapper").hide();
    mode = "seqEvent";
    hideUtilButtons();
}

function selectedEventTarget(charCode) {
    showUtilButtons();
    $("#sequenceWrapper").show();
    mode = "sequenceEditor";
    if(charCode !== undefined) {
        sequences[curSeq].push({
            "char": parseInt(charCode),
            "start": 0,
            "end": 1,
        });
        redrawTimeline();
    }
}

function removeTlEvent() {
    selectedEvents.forEach(function(id) {
        delete sequences[curSeq][id];
    });
    sequences[curSeq].sort();
    sequences[curSeq].length -= selectedEvents.length;
    selectedEvents = [];
    redrawTimeline();
    hideDigitalControls();
}

function snapSettingChange() {
    if($("#snapTo").val() == "seconds") {
        $("#snapSeconds").show();
    } else {
        $("#snapSeconds").hide();
    }
    addSnap();
}

function getSnapSettings() {
    return {
        snap: $("#snap")[0].checked,
        mode: $("#snapTo").val() == "seconds",
        stepSize: parseFloat($("#snapSeconds").val()),
    };
}

function addSnap() {
    var snapSettings = getSnapSettings();
    if(snapSettings.snap) {
        if(snapSettings.mode) {
            var secondsPerPixel = editorScale / $(".timelineElement").width();
            $(".timelineBar").draggable({
                snap: undefined,
                grid: [snapSettings.stepSize / secondsPerPixel, 0],
            }).resizable({
                snap: undefined,
                grid: [snapSettings.stepSize / secondsPerPixel, 0],
            });
        } else {
            $(".timelineBar").draggable({
                snap: ".timelineBar",
                grid: undefined,
            }).resizable({
                snap: ".timelineBar",
                grid: undefined,
            });
        }
    } else {
        $(".timelineBar").draggable({
            snap: undefined,
            grid: undefined,
        }).resizable({
            snap: undefined,
            grid: undefined,
        });
    }
}

function sequenceKeyEvent(event) {
    if(event == 65) {
        if(selectedEvents.length < sequences[curSeq].length) {
            selectedEvents = Array.from(sequences[curSeq], function(x,i) { return i; });
        } else {
            selectedEvents = [];
        }
        redrawBars();
    } else if(event == 27) {
        if(isDragging) {
            cancelDragging = true;
        } else {
            selectedEvents = [];
            redrawBars();
        }
    } else if(event == 46) {
        removeTlEvent();
    }
}

function recordEvents() {
    sequenceRecording = true;
    selectedEvents = [];
    $(".frontWrapper").hide();
    mode = "play";
    hideUtilButtons();
    $("#headerCancelButton > span").text("Save");
}

function addRecordedEvent(event) {
    if(startEventRecord === undefined) {
        startEventRecord = event.timeStamp;
    }
    if(event.type == "keyup") {
        for(var i = 0; i < newEvents.length; i++) {
            if(newEvents[i].upEvent === undefined && newEvents[i].keyCode == event.keyCode) {
                newEvents[i].upEvent = event;
                return;
            }
        }
    } else {
        newEvents.push(event);
    }
}

function finishRecording() {
    sequenceRecording = false;
    showUtilButtons();
    $("#sequenceWrapper").show();
    mode = "sequenceEditor";
    $("#headerCancelButton > span").text("Cancel");

    selectedEvents = Array.from(newEvents, function(x,i) { return i + sequences[curSeq].length; });
    newEvents.forEach(function(event) {
        if(bindings[event.keyCode] === undefined) {
            return;
        }
        sequences[curSeq].push({
            "char": event.keyCode,
            "start": (event.timeStamp - startEventRecord) / 1000,
            "end": (event.upEvent.timeStamp - startEventRecord) / 1000,
        });
    });
    newEvents = [];
    startEventRecord = undefined;
    redrawTimeline();
}

function roundToLeftmost(number) {
    var n = Math.floor(Math.log10(number));
    return Math.round(number / Math.pow(10, n)) * Math.pow(10, n);
}

$(window).resize(function(event) {
    if(event.target == window) {
        redrawTimeline();
    }
});

function playSequence(key) {
    if(sounds[key].length > 0) {
        stopSequence(key);
        sounds[key] = [];
        return;
    }

    var seqName = bindings[key].sequence;
    var seq = sequences[seqName];
    if(seq === undefined) {
        return;
    }
    setPlaying(key, 1);
    var seqLength = Math.max.apply(Math, seq.map(function(element) { return element.end; }));
    var startTime = ctx.currentTime;

    var soundObj = {
        startTime: startTime,
        currentlyPlaying: [],
        stop: function() {
            stopSequence(key);
        },
    };
    sounds[key].push(soundObj);

    var handleEvent = function(event) {
        if(!isSameIteration(key, startTime)) {
            return;
        }
        keyPressed(event.char);
        sounds[key][0].currentlyPlaying.push(event.char);
        setTimeout(function() {
            if(!isSameIteration(key, startTime)) {
                return;
            }
            keyReleased(event.char);
            sounds[key][0].currentlyPlaying.splice(sounds[key][0].currentlyPlaying.indexOf(event.char));
        }, (event.end - event.start) * 1000);
        setTimeout(function() { handleEvent(event); }, seqLength * 1000);
    };
    seq.forEach(function(event) {
        setTimeout(function() { handleEvent(event); }, event.start * 1000);
    });
}

function isSameIteration(key, time) {
    return sounds[key].length > 0 && sounds[key][0].startTime === time;
}

function stopSequence(key) {
    setPlaying(key, 0);
    sounds[key][0].startTime = 0;
    sounds[key][0].currentlyPlaying.forEach(function(key) {
        keyReleased(key);
    });
}
