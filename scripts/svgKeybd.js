function appendCenterKey(leftOffset, onclick, id) {
    appendKey('m ' + leftOffset + ',200 60,0 0,-100 -15,0 0,-100 -30,0 0,100 -15,0 z', onclick, id);
}
function appendLeftKey(leftOffset, onclick, id) {
    appendKey('m ' + leftOffset + ',200 60,0 0,-100 -15,0 0,-100 -45,0 z', onclick, id);
}
function appendRightKey(leftOffset, onclick, id) {
    appendKey('m ' + leftOffset + ',200 60,0 0,-200 -45,0 0,100 -15,0 z', onclick, id);
}
function appendSecondaryKey(leftOffset, onclick, id) {
    appendKey('m ' + (leftOffset + 45) + ',100 30,0 0,-100 -30,0 z', onclick, id, '#000000');
}
function appendKey(d, onclick, id, fill) {
    fill = fill ? fill : "#FFFFFF";
    var key = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    key.setAttribute('fill', fill);
    key.setAttribute('stroke', '#000000');
    key.setAttribute('d', d);
    key.setAttribute('onclick', onclick);
    key.setAttribute('id', "pianoKey" + id);
    document.getElementById("mainKey1").appendChild(key);
}

function loadPianoKeys() {
    var offset = 0;
    var keyNumber = Math.floor($(window).width() / 60 - 1);
    var totalKeyNumber = Math.floor(keyNumber / 7) * 12;
    additionalNumber = [0, 2, 4, 6, 8, 10, 11];
    totalKeyNumber += additionalNumber[(keyNumber % 7)];
    if(totalKeyNumber > 120) {
        totalKeyNumber = 120;
    }
    var midiCode = Math.ceil(60 - totalKeyNumber / 2);
    subtractions = [0, 1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5];
    var startKey = midiCode % 12;
    startKey -= subtractions[startKey];
    var endKey = keyNumber + startKey;
    var firstKey = 1;
    midiCode += 256;
    minButton = midiCode;
    for(var i = startKey; i < endKey; i++) {
        if(midiCode - 256 >= 120) {
            break;
        }
        if(i % 7 === 0 || (i - 3) % 7 === 0) {
            if(firstKey) {
                minButton = midiCode -= 1;
            }
            appendLeftKey(offset, 'changeOrAddSettings(' + midiCode + ')', midiCode);
            midiCode += 1;
            appendSecondaryKey(offset, 'changeOrAddSettings(' + midiCode + ')', midiCode);
        } else if((i - 2) % 7 === 0 || (i + 1) % 7 === 0) {
            if(firstKey) {
                appendSecondaryKey(-60, 'changeOrAddSettings(' + (midiCode - 1) + ')', midiCode - 1);
                minButton = midiCode - 1;
            }
            appendRightKey(offset, 'changeOrAddSettings(' + midiCode + ')', midiCode);
        } else {
            if(firstKey) {
                minButton = midiCode - 1;
                appendSecondaryKey(-60, 'changeOrAddSettings(' + (midiCode - 1) + ')', midiCode - 1);
            }
            appendCenterKey(offset, 'changeOrAddSettings(' + midiCode + ')', midiCode);
            midiCode += 1;
            appendSecondaryKey(offset, 'changeOrAddSettings(' + midiCode + ')', midiCode);
        }
        midiCode += 1;
        offset += 60;
        firstKey = 0;
    }
    maxButton = midiCode - 1;
    $("#mainKey1").css("width", offset);
}

window.onresize = redrawPianoBoard;
function redrawPianoBoard() {
    var keys = [];
    var newKeys = [];
    var charCode;
    if(localStorage.getItem("pianoMode") == "true") {
        $(".mainKey > div").remove();
        $("#mainKey1").children().remove();
        keys = Object.keys(bindings).filter(function(element) {
            return element >= minButton && element <= maxButton;
        });
        loadPianoKeys();
        newKeys = Object.keys(bindings).filter(function(element) {
            return element >= minButton && element <= maxButton;
        });
        $(newKeys).not(keys).get().forEach(function(key) {
            $("#btn" + key).remove();
        });
        $.unique(keys.concat(newKeys)).forEach(function(key) {
            createButton(key, buttonTextFromBinding(bindings[key]));
        });
        loadLayout();
    }
}

$("body").ready(loadPianoKeys);

function changeOrAddSettings(charCode) {
    if(!bindings[charCode]) {
        registerButton(charCode);
    }
    changeSettings(charCode);
}

function displayOnPiano(charCode, audioText) {
    $("#pianoLabel" + charCode).remove();
    leftWidth = ($(window).width() - $("#mainKey1").width()) / 2;
    
    numKeys = charCode - minButton;
    leftWidth += Math.floor(numKeys / 12) * 7 * 60;
    numKeys %= 12;
    for(var i = 1; i <= numKeys; i++) {
        if(!isBlackKey(charCode - i)) {
            leftWidth += 60;
        }
    }

    var label = $('<div class="pianoLabelWrapper" id="pianoLabel' + charCode + '" onclick="changeOrAddSettings(' + charCode + ')"><p class="pianoLabel">' + getButtonName(charCode) + ': ' + audioText + '</p></div>');
    if(isBlackKey(charCode)) {
        label.addClass("blackKeyLabel");
        if(charCode != minButton) {
            leftWidth -= 7.5;
        }
        if(charCode == maxButton) {
            leftWidth -= 7.5;
        }
    } else {
        leftWidth += 22.5;
    }
    label.css("left", leftWidth);
    $("div.mainKey").append(label);
}

function isBlackKey(charCode) {
    charCode -= 256;
    var num = charCode % 12;
    return $.inArray(num, [1, 3, 6, 8, 10]) + 1;
}

function removePianoButton(charCode) {
    $("#pianoLabel" + charCode).remove();
    setPlaying(charCode, 0);
}
