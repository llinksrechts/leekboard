var soundPreview;

function shouldPreview() {
    return mode == "settings" && localStorage.getItem("livePreview") == "true" && ((!soundNeedsReload && bindings[curSettings].audio !== undefined) || $("#modeSelect").val() == "met");
}

function preview() {
    if(soundPreview !== undefined) {
        soundPreview.stop();
        soundPreview = undefined;
    }
    if(shouldPreview()) {
        if($("#modeSelect").val() == "met") {
            previewMet();
        } else if($("#modeSelect").val() == "sync") {
            previewSync();
        } else {
            previewSound();
        }
    }
}

function previewMet() {
    var bpm = $("#bpmSelect").val();
    var beats = $("#beatsSelect").val();
    var vol = $("#volSlider").slider("values", 0);
    var playTime = Math.max(4, beats) * 60000 / bpm;
    var sound = playMet(vol, bpm, beats);
    soundPreview = sound;
    setTimeout(function() {
        if(sound == soundPreview) {
            sound.stop();
            soundPreview = undefined;
        }
    }, playTime);
}

function previewSound() {
    var arrBuff = bindings[curSettings].audio;
    var vol = $("#volSlider").slider("values", 0);
    var start = $("#slider").slider("values", 0) / 100;
    var stop = $("#slider").slider("values", 1) / 100;
    var pitch = $("#pitchSlider").slider("values", 0); 
    var sound = playSound(arrBuff, vol, false, stop, start, pitch);
    soundPreview = sound;
    setTimeout(function() {
        if(sound == soundPreview) {
            sound.stop();
            soundPreview = undefined;
        }
    }, 1500);
}

function previewSync() {
    var met = playMet(100, 90, 1);
    met.sourceMet = {
        "bpm": 90,
        "finish": false,
        "beats": 1,
    };
    met.startTime = ctx.currentTime;
    metronome.push(met);
    var arrBuff = bindings[curSettings].audio;
    var vol = $("#volSlider").slider("values", 0);
    var start = $("#slider").slider("values", 0) / 100;
    var stop = $("#slider").slider("values", 1) / 100;
    var pitch = $("#pitchSlider").slider("values", 0); 
    var deviation = $("#timeSlider").slider("values", 0);
    var sound = playSync(arrBuff, vol, true, stop, start, deviation, pitch);

    var playTime = Math.max(4, Math.ceil(((stop - start) * arrBuff.duration + 0.1) * (3 + deviation / 200))) * 2000 / 3;

    var handler = {
        "stop": function() {
            met.stop();
            sound.stop();
            metronome.splice(metronome.indexOf(met));
        }
    };
    soundPreview = handler;
    setTimeout(function() {
        if(handler == soundPreview) {
            handler.stop();
            soundPreview = undefined;
        }
    }, playTime);
}
