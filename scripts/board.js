// TODO: preview end of sample on end time change
window.onerror = function() {
    window.location = "notSupported.html";
};
var mode = "play";
var recording = false;
var curSettings;
var soundNeedsReload = false;
var soundIsReloading = false;
$(".frontWrapper").hide();
var ctx = new window.AudioContext();
var lastReload = ctx.currentTime;
var sounds = [];
var soundCount = [];
var bindings = {};
var sequences = {};
var keyDown = [];
var syncTarget;
var ultimateSyncTarget;
var analyser;
var minButton;
var maxButton;
var recorder = new Recorder(ctx);
var metMajor;
var metMinor;
var metronome = [];
$("body").ready(function() {
    if(navigator.userAgent.includes("Chrome") && navigator.userAgent.includes("Android")) {
        $("body").css("background-position", "center -50%");
    }
    if(window.outerWidth < 964) {
        $(".keySettings").css("width", "80%").css("left", "10%");
    }
    $("#slider").slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        change: preview,
    });
    $("#timeSlider").slider({
        min: -100,
        max: 100,
        values: [0],
        change: preview,
    });
    $("#volSlider").slider({
        min: 0,
        max: 100,
        values: [100],
        change: preview,
    });
    $("#pitchSlider").slider({
        min: 0,
        max: 200,
        values: [100],
        change: preview,
    });
    $("#settings").draggable();
    $("#sequenceEditor").draggable();
    $("#headerCancel").hide();
    loadMetSounds();
});
function loadMetSounds() {
    var getSoundMa = new XMLHttpRequest(), getSound = new XMLHttpRequest();
    getSoundMa.open("GET", "sounds/plopp.ogg", true);
    getSoundMa.responseType = "arraybuffer";
    getSoundMa.onload = function() {
        ctx.decodeAudioData(getSoundMa.response, function(buffer) {
            metMajor = buffer;
        });
    };
    getSoundMa.send();
    getSound.open("GET", "sounds/blubb.ogg", true);
    getSound.responseType = "arraybuffer";
    getSound.onload = function() {
        ctx.decodeAudioData(getSound.response, function(buffer) {
            metMinor = buffer;
        });
    };
    getSound.send();
}

$("body").keydown(function(key) {
    if(keyDown[key.keyCode]) {
        return;
    }
    keyDown[key.keyCode] = true;
    if(sequenceRecording) {
        addRecordedEvent(key);
    }
    keyPressed(key.keyCode);
    if(mode == "sequenceEditor" && [65, 46, 27].indexOf(key.keyCode) != -1) {
        return false;
    }
});
function keyPressed(key, velocity, isPiano) {
    velocity = velocity === undefined ? 1 : velocity;
    switch (mode) {
    case "play":
        var sound;
        if(bindings[key] !== undefined && bindings[key].mode === "alias") {
            if(bindings[key].forAlias !== undefined) {
                key = bindings[key].forAlias;
            } else {
                break;
            }
        }
        if(bindings[key] !== undefined && (bindings[key].audio || bindings[key].mode === "met" || bindings[key].mode === "sequence")) {
            switch (bindings[key].mode) {
            case "repeat":
                if(sounds[key].length === 0) {
                    sounds[key].push(playSound(bindings[key].audio, bindings[key].volume * velocity, true, bindings[key].stop, bindings[key].start, bindings[key].pitch));
                    sounds[key][0].velocity = velocity;
                    syncTarget = sounds[key][0];
                    setPlaying(key, 1);
                } else {
                    sound = sounds[key].splice(0, 1)[0];
                    if(sound === syncTarget) {
                        syncTarget = undefined;
                    }
                    if(!bindings[key].finish) {
                        soundCount[key] = soundCount[key] + 1 || 1;
                        sound.gain.gain.linearRampToValueAtTime(0, ctx.currentTime + bindings[key].decay);
                        window.setTimeout(function() {
                            soundCount[key] -= 1;
                            sound.stop();
                            if(!soundCount[key] && !sounds[key].length) {
                                setPlaying(key, 0);
                            }
                        }, bindings[key].decay * 1000);
                    } else {
                        var rstopTime = ctx.currentTime, rtime = (sound.context.currentTime - sound.startTime) % sound.buffer.duration;
                        playSound(sound.buffer, bindings[key].volume * (sound.velocity || 1), false, bindings[key].stop, rtime, bindings[key].pitch);
                        sound.stop();
                        soundCount[key] = soundCount[key] + 1 || 1;
                        rstopTime += sound.buffer.duration - rtime;
                        window.setTimeout(function() {
                            soundCount[key] -= 1;
                            if(!soundCount[key] && !sounds[key].length) {
                                setPlaying(key, 0);
                            }
                        }, (rstopTime - ctx.currentTime) * 1000);
                    }
                }
                break;
            case "song":
                if(sounds[key].length !== 0) {
                    if(bindings[key].finish) {
                        break;
                    }
                    sound = sounds[key].splice(0, 1)[0];
                    if(ultimateSyncTarget === sound) {
                        ultimateSyncTarget = undefined;
                        if(syncTarget === sound) {
                            syncTarget = undefined;
                        }
                    }
                    sound.gain.gain.linearRampToValueAtTime(0, ctx.currentTime + bindings[key].decay);
                    window.setTimeout(function() {
                        soundCount[key] -= 1;
                        sound.stop();
                        if(!soundCount[key]) {
                            setPlaying(key, 0);
                        }
                    }, bindings[key].decay * 1000);
                    sounds[key] = [];
                    break;
                }
                /* falls through */
            case "once":
                if(sounds[key].length === 0 || bindings[key].finish || sounds[key][0].ended) {
                    soundCount[key] = soundCount[key] + 1 || 1;
                    if(!bindings[key].finish) {
                        sounds[key] = [];
                    }
                    sounds[key].push(playSound(bindings[key].audio, bindings[key].volume * velocity, false, bindings[key].stop, bindings[key].start, bindings[key].pitch));
                    sound = sounds[key][sounds[key].length - 1];
                    setPlaying(key, 1);
                    if(bindings[key].mode == "song") {
                        ultimateSyncTarget = sound;
                    }
                    window.setTimeout(function() {
                        if(sounds[key].indexOf(sound) != -1) {
                            soundCount[key] -= 1;
                            sounds[key].splice(sounds[key].indexOf(sound), 1);
                            if(!soundCount[key]) {
                                setPlaying(key, 0);
                            }
                        }
                    }, bindings[key].audio.duration * 1000);
                }
                break;
            case "hold":
                if(sounds[key].length === 0) {
                    setPlaying(key, 1);
                    sounds[key].push(playSound(bindings[key].audio, bindings[key].volume * velocity, true, bindings[key].stop, bindings[key].start, bindings[key].pitch));
                    sounds[key][0].velocity = velocity;
                }
                break;
            case "sync":
                if(sounds[key].length === 0) {
                    var node = playSync(bindings[key].audio, bindings[key].volume * velocity, true, bindings[key].stop, bindings[key].start, bindings[key].deviation, bindings[key].pitch);
                    if(node !== undefined) {
                        node.velocity = velocity;
                        sounds[key].push(node);
                        setPending(key, 1);
                        window.setTimeout(function() {
                            if(sounds[key].indexOf(node) != -1) {
                                setPending(key, 0);
                                setPlaying(key, 1);
                                soundCount[key] = soundCount[key] + 1 || 1;
                            }
                        }, (node.startTime - ctx.currentTime) * 1000);
                    }
                } else {
                    sound = sounds[key].splice(0, 1)[0];
                    var sstopTime = ctx.currentTime;
                    if(bindings[key].finish) {
                        var stime = (sound.context.currentTime - sound.startTime) % sound.buffer.duration;
                        if(stime < 0) {
                            setPending(key, 0);
                        } else {
                            playSound(sound.buffer, bindings[key].volume * sound.velocity, false, bindings[key].stop, stime, bindings[key].pitch);
                            sstopTime += sound.buffer.duration - stime;
                            window.setTimeout(function() {
                                soundCount[key] -= 1;
                                if(!soundCount[key]) {
                                    setPlaying(key, 0);
                                }
                            }, (sstopTime - ctx.currentTime) * 1000);
                        }
                        sound.stop();
                    } else {
                        if(sound.startTime >= ctx.currentTime) {
                            setPending(key, 0);
                            sound.stop();
                            break;
                        }
                        sound.gain.gain.linearRampToValueAtTime(0, ctx.currentTime + bindings[key].decay);
                        window.setTimeout(function() {
                            soundCount[key] -= 1;
                            sound.stop();
                            if(!soundCount[key]) {
                                setPlaying(key, 0);
                            }
                        }, bindings[key].decay * 1000);
                    }
                }
                break;
            case "met":
                if(sounds[key].length !== 0) {
                    if(metronome.indexOf(sounds[key][0]) != -1) {
                        metronome.splice(metronome.indexOf(sounds[key][0]), 1);
                    }
                    sounds[key][0].stop();
                    sounds[key] = [];
                    setPlaying(key, 0);
                } else {
                    sounds[key].push(playMet(bindings[key].volume, bindings[key].bpm, bindings[key].beats));
                    metronome.push(sounds[key][0]);
                    sounds[key][0].sourceMet = bindings[key];
                    sounds[key][0].startTime = ctx.currentTime;
                    setPlaying(key, 1);
                }
                break;
            case "sequence":
                playSequence(key);
                break;
            }
        }
        break;
    case "register":
        hideSettings();
        registerButton(key);
        break;
    case "changeKey":
        hideSettings();
        mode = "settings";
        $("#settingsWrapper").show();
        changeButtonKey(key);
        break;
    case "sequenceEditor":
        sequenceKeyEvent(key);
        break;
    }
}
$("body").keyup(function(key) {
    keyDown[key.keyCode] = false;
    if(sequenceRecording) {
        addRecordedEvent(key);
    }
    if(mode != "play") {
        return;
    }
    if(bindings[key.keyCode] !== undefined) {
        keyReleased(key.keyCode);
    }
});
function keyReleased(key) {
    if(bindings[key].mode == "alias" && bindings[key].forAlias !== undefined) {
        key = bindings[key].forAlias;
        if(bindings[key] === undefined) {
            return;
        }
    }
    if(bindings[key].mode == "hold") {
        var sound;
        if(!bindings[key].finish) {
            if(sounds[key][0]) {
                sound = sounds[key].splice(0, 1)[0];
                soundCount[key] = soundCount[key] + 1 || 1;
                sound.gain.gain.linearRampToValueAtTime(0, ctx.currentTime + bindings[key].decay);
                window.setTimeout(function() {
                    sound.stop();
                    soundCount[key] -= 1;
                    if(!soundCount[key]) {
                        setPlaying(key, 0);
                    }
                }, bindings[key].decay * 1000);
            } else {
                setPlaying(key, 0);
            }
        } else {
            var stopTime = ctx.currentTime;
            sound = sounds[key].splice(0, 1)[0];
            soundCount[key] = soundCount[key] + 1 || 1;
            var time = (sound.context.currentTime - sound.startTime) % sound.buffer.duration;
            playSound(sound.buffer, bindings[key].volume * sound.velocity, false, bindings[key].stop, time, bindings[key].pitch);
            sound.stop();
            stopTime += sound.buffer.duration - time;
            window.setTimeout(function() {
                if(!sounds[key].length) {
                    soundCount[key] -= 1;
                    if(!soundCount[key]) {
                        setPlaying(key, 0);
                    }
                }
            }, (stopTime - ctx.currentTime) * 1000);
        }
        sounds[key] = [];
    }
}

function setPlaying(charCode, isPlaying) {
    setStatus(charCode, isPlaying, "#afa", "playing", "#040");
}
function setPending(charCode, isPending) {
    setStatus(charCode, isPending, "#ff8", "pending", "#770");
}
function setError(charCode, isError) {
    setStatus(charCode, isError, "#faa", "noSound", "#400");
}
function updateReloadButton(isReloading, needsReload) {
    $("#refreshSource").prop("disabled", isReloading);
    if(needsReload) {
        $("#refreshSource").addClass("pending");
    } else {
        $("#refreshSource").removeClass("pending");
        soundNeedsReload = false;
    }
    if(isReloading) {
        $("#refreshSource").addClass("noSound");
    } else {
        $("#refreshSource").removeClass("noSound");
        lastReload = ctx.currentTime;
    }
}
function setStatus(charCode, isStatus, hexCode, cssClass, blackHexCode) {
    if($("#btn" + charCode).length === 0) {
        if(isStatus) {
            if(isBlackKey(charCode)) {
                document.getElementById('pianoKey' + charCode).attributes.fill.value = blackHexCode;
            } else {
                document.getElementById('pianoKey' + charCode).attributes.fill.value = hexCode;
            }
        } else {
            if(isBlackKey(charCode)) {
                document.getElementById('pianoKey' + charCode).attributes.fill.value = "#000";
            } else {
                document.getElementById('pianoKey' + charCode).attributes.fill.value = "#fff";
            }
        }
    } else {
        if(isStatus) {
            $("#btn" + charCode).addClass(cssClass);
        } else {
            $("#btn" + charCode).removeClass(cssClass);
        }
    }
}
function registerButton(charCode) {
    mode = "play";
    if(bindings[charCode] !== undefined) {
        return;
    }
    bindings[charCode] = {};
    bindings[charCode].mode = "hold";
    bindings[charCode].finish = false;
    bindings[charCode].start = 0;
    bindings[charCode].stop = 1;
    bindings[charCode].volume = 100;
    bindings[charCode].pitch = 100;
    bindings[charCode].decay = 0.5;
    bindings[charCode].deviation = 0;
    sounds[charCode] = [];
    createButton(charCode);
    saveBindings();
}
function changeButtonKey(charCode) {
    if(bindings[charCode] !== undefined) {
        return;
    }
    for(var key in bindings) {
        if(bindings[key].forAlias == curSettings) {
            bindings[key].forAlias = charCode;
            $("#btn" + key + " > span").html("Alias: " + getButtonName(charCode) + "<br>" + getButtonName(key));
        }
    }
    keyBindings = bindings[curSettings];
    delete bindings[curSettings];
    delete sounds[curSettings];
    var button = document.getElementById("btn" + curSettings);

    curSettings = charCode;
    if(button === null) {
        createButton(curSettings, buttonTextFromBinding(keyBindings));
        button = document.getElementById("btn" + curSettings);
    }
    bindings[curSettings] = keyBindings;
    sounds[curSettings] = [];

    button.id = "btn" + curSettings;
    button.innerHTML = button.innerHTML.split("<br>")[0] + "<br>" + getButtonName(curSettings);
    button.onclick = function() {
        if($(this).hasClass('noclick')) {
            $(this).removeClass('noclick');
        } else {
            changeSettings(charCode);
        }
    };
    redrawPianoBoard();
    if(charCode >= minButton && charCode <= maxButton) {
        button.remove();
    }
}
function toggleRecord() {
    recording = !recording;
    if(recording) {
        $("#recordIcon").animate({
            "border-radius": "-=50%"
        });
        recorder.record();
    } else {
        $("#recordIcon").animate({
            "border-radius": "+=50%"
        });
        recorder.stop();
        recorder.exportWAV(function(blob) {
            Recorder.forceDownload(blob);
        });
        recorder.clear();
    }
}
function calculateSync(buff, isSong, callback) {
    if(isSong) {
        calculateSongSync(buff, callback);
        return;
    }
    var arr = buff.getChannelData(0);
    var time = 0;
    for(var i = 0; i < arr.length; i++) {
        if(arr[i] > arr[time]) {
            time = i;
        }
    }
    buff.peak = time / buff.sampleRate;
    return;

    /* peak analyser // TODO: do we need this?
       var node = ctx.createBufferSource();
       node.buffer = buff;
       node.loop = true;
       analyser = ctx.createAnalyser();
       analyser.fftSize = 1024;
       node.connect(analyser);
       node.start();
       var arr = new Uint8Array(analyser.frequencyBinCount);
       setTimeout(function() {
       analyser.getByteFrequencyData(arr);
       var time = 0;
       for(var i = 0; i < arr.length; i++) {
       if(arr[i] > arr[time]) {
       time = i;
       }
       }
       buff.peak = time;
       buff.peaks = arr;
       node.stop();
       if(callback) {
       callback(node);
       }
       }, buff.duration); */
}
function calculateSongSync(buff, callback) {
    var octx = new OfflineAudioContext(1, buff.length, buff.sampleRate);
    var node = octx.createBufferSource();
    node.buffer = buff;
    
    var filter = octx.createBiquadFilter();
    filter.type = "lowpass";
    node.connect(filter);
    
    filter.connect(octx.destination);
    node.start(0);
    octx.startRendering();
    octx.oncomplete = function(e) {
        var filteredBuff = e.renderedBuffer;
        var arr = filteredBuff.getChannelData(0);
        var sum = arr.reduce(function(a, b) { return a + b; });
        var avg = sum / arr.length;
        var peaks = getPeaksAtThreshold(arr, avg, filteredBuff.sampleRate);
        var intervals = countIntervalsBetweenNearbyPeaks(peaks, filteredBuff.sampleRate);
        var absoluteTempo = getAbsoluteTempo(intervals, filteredBuff.sampleRate);
        buff.peak = absoluteTempo.firstPeak / filteredBuff.sampleRate;
        buff.peaks = peaks;
        buff.kickRate = absoluteTempo.length;
        return;
    };
}
function getPeaksAtThreshold(data, threshold, sampleRate) {
    var peaksArray = [];
    var length = data.length;
    for(var i = 0; i < length; i++) {
        if(data[i] > threshold) {
            peaksArray.push(i);
            i += Math.floor(sampleRate / 4.4);
        }
    }
    return peaksArray;
}
function countIntervalsBetweenNearbyPeaks(peaks, sampleRate) {
    var intervalCounts = [];
    for(var i = 0; i < peaks.length - 1; i++) {
        var interval = peaks[i + 1] - peaks[i];
        while(interval / sampleRate < 1 / 3) { interval *= 2; }
        while(interval / sampleRate > 2 / 3) { interval /= 2; }
        for(var j = 0; j < intervalCounts.length; j++) {
            if(intervalCounts[j].interval * 1.3 > interval && interval * 1.3 > intervalCounts[j].interval) {
                intervalCounts[j].interval = (intervalCounts[j].interval * intervalCounts[j].amount + interval) / ++intervalCounts[j].amount;
                break;
            }
        }
        intervalCounts.push({ interval: interval, amount: 1, firstPeak: peaks[i] });
    }
    return intervalCounts;
}
function getAbsoluteTempo(intervals, sampleRate) {
    var maxAmount = intervals[intervals.map(function(o){return o.amount;}).indexOf(Math.max.apply(intervals,intervals.map(function(o){return o.amount;})))];
    maxAmount.speed = 60 / (maxAmount.interval / sampleRate);
    maxAmount.length = maxAmount.interval / sampleRate;
    return maxAmount;
}

function playSound(arrBuff, volume, repeat, noteOff, noteOn, pitch) {
    if(repeat === undefined) { repeat = true; }
    if(noteOn === undefined) { noteOn = 0; }
    if(noteOff === undefined) { noteOff = 1; }
    noteOn *= arrBuff.duration;
    noteOff *= arrBuff.duration;
    var src = ctx.createBufferSource();
    src.buffer = arrBuff;
    src.loop = repeat;
    src.len = noteOff - noteOn;
    var p = pitch / 100 - 1;
    if(p < 0) {
        p /= 2;
    }
    p += 1;
    src.playbackRate.value = p;

    var gain = ctx.createGain();
    gain.gain.value = volume / 100.0;
    src.connect(gain);
    src.gain = gain;
    gain.connect(ctx.destination);

    src.onended = function() {
        src.ended = true;
        gain.disconnect();
    };

    if(repeat) {
        src.loopStart = noteOn;
        src.loopEnd = noteOff;
    }

    src.startTime = ctx.currentTime;
    src.noteOn = noteOn;

    src.start(0, noteOn, noteOff - noteOn);
    if(recording) {
        recorder.addInput(src);
    }
    return src;
}
function playSync(arrBuff, volume, repeat, noteOff, noteOn, deviation, pitch) {
    // TODO: syncing to song doesnt quite work
    // TODO: song key doesnt turn gray after finishing playing
    if(repeat === undefined) { repeat = true; }
    if(noteOn === undefined) { noteOn = 0; }
    if(noteOff === undefined) { noteOff = 1; }
    if(deviation === undefined) { deviation = 1; }
    if(ultimateSyncTarget !== undefined) { syncTarget = ultimateSyncTarget; }
    if(metronome.length === 0 && syncTarget === undefined) { return; }
    var origNoteOn = noteOn;
    noteOn *= arrBuff.duration;
    noteOff *= arrBuff.duration;
    var src = ctx.createBufferSource();
    src.loop = repeat;
    var p = pitch / 100 - 1;
    if(p < 0) {
        p /= 2;
    }
    p += 1;
    src.playbackRate.value = p;

    var metLast = metronome.length ? metronome[metronome.length - 1] : undefined;
    
    var songSync = metLast ? 60 / metLast.sourceMet.bpm : syncTarget.buffer.kickRate;

    if(metLast && metLast.sourceMet.finish) {
        songSync *= metLast.sourceMet.beats;
    }

    var len = noteOff - noteOn;
    var syncLen = songSync ? songSync : syncTarget.len;

    var syncRate = metLast ? 1 : syncTarget.playbackRate.value;
    syncLen /= syncRate;
    var untilNextKick = syncLen - ((ctx.currentTime - ((metLast ? 0.1414095 /* duration until metronome kick */ : (syncTarget.buffer.peak - syncTarget.noteOn) / syncRate) + (metLast ? metLast.startTime : syncTarget.startTime))) % syncLen);
    var delayTime = untilNextKick - arrBuff.peak + noteOn + len * deviation / 200;
    delayTime %= syncLen;

    var gain = ctx.createGain();
    gain.gain.value = volume / 100.0;
    src.connect(gain);
    src.gain = gain;
    gain.connect(ctx.destination);

    if(metLast && len < syncLen) {
        var higher = 1;
        for(var i = 2; i <= metLast.sourceMet.beats; i++) {
            if(metLast.sourceMet.beats % i === 0 && syncLen / i > len - 0.1) {
                higher = i;
            }
        }
        syncLen /= higher;
    }

    var length = syncLen - len;
    while(length < -0.1) {
        length += syncLen;
    }

    var numChannel = arrBuff.numberOfChannels;
    var newBuff = ctx.createBuffer(numChannel, (len + length) * arrBuff.sampleRate * p, arrBuff.sampleRate);
    for(var j = 0; j < numChannel; j++) {
        var channelData = arrBuff.getChannelData(j);
        newBuff.getChannelData(j).set(channelData.subarray(Math.max(origNoteOn * channelData.length, channelData.length - newBuff.getChannelData(j).length), len * arrBuff.sampleRate + origNoteOn * channelData.length), 0);
    }

    src.buffer = newBuff;

    src.onended = function() {
        src.ended = true;
        gain.disconnect();
    };

    src.startTime = ctx.currentTime + delayTime;

    src.start(ctx.currentTime + delayTime);
    if(recording) {
        recorder.addInput(src);
    }
    return src;
}
function playMet(volume, bpm, beats) {
    var src = ctx.createBufferSource();
    src.loop = true;
    var gain = ctx.createGain();
    gain.gain.value = volume / 100.0;
    src.connect(gain);
    gain.connect(ctx.destination);

    var channels = metMinor.numberOfChannels;
    var beatLen = 60 / bpm * metMinor.sampleRate;
    var metBuff = ctx.createBuffer(channels, beatLen * beats, metMinor.sampleRate);
    for(var i = 0; i < channels; i++) {
        metBuff.getChannelData(i).set((beats == 1 ? metMinor : metMajor).getChannelData(i).subarray(0, beatLen), 0);
        for(var j = 1; j < beats; j++) {
            metBuff.getChannelData(i).set(metMinor.getChannelData(i).subarray(0, beatLen), j * beatLen);
        }
    }
    src.buffer = metBuff;
    src.start();
    return src;
}
function register() {
    mode = "register";
    $("#newButton").blur();
    $(".frontWrapper").hide();
    $("#registerWrapper").show();
}
function changeKey() {
    mode = "changeKey";
    $("#changeButton").blur();
    $(".frontWrapper").hide();
    $("#changeKeyWrapper").show();
}
function viewGuide() {
    mode = "guide";
    $("#guide").blur();
    $(".frontWrapper").hide();
    $("#guideWrapper").show();

}
function createButton(charCode, audioText) {
    if(audioText === undefined) { audioText = buttonText(); }
    if(localStorage.getItem("pianoMode") == "true") {
        if(minButton <= charCode && maxButton >= charCode) {
            displayOnPiano(charCode, audioText);
            return;
        }
    }
    var newBtn = document.createElement("button");
    newBtn.innerHTML = audioText + "<br>" + getButtonName(charCode);
    newBtn.id = "btn" + charCode;
    newBtn.className = "key";
    newBtn.onclick = function() {
        if($(this).hasClass('noclick')) {
            $(this).removeClass('noclick');
        } else {
            changeSettings(charCode);
        }
    };
    document.getElementById('buttonContainer').appendChild(newBtn);
    $("#btn" + charCode).button().draggable({
        cancel: false,
        start: function(event, ui) {
            $(this).addClass('noclick');
        }
    });
}
function getButtonName(charCode) {
    if(keynames[charCode]) {
        return keynames[charCode];
    } else if(charCode > 255) {
        return getNoteName(charCode - 256);
    }
}
function editButton(charCode, audioText) {
    if(localStorage.getItem("pianoMode") == "true") {
        if(minButton <= charCode && maxButton >= charCode) {
            displayOnPiano(charCode, audioText);
            return;
        }
    }
    $("#btn" + charCode).html(audioText + "<br>" + getButtonName(charCode));
}
function buttonTextFromBinding(binding) {
    return binding.mode == "met" ? (binding.bpm || 90) + " BPM." : (
        binding.mode == "alias" ? "Alias: " + (binding.forAlias === undefined ? "None" : getButtonName(binding.forAlias)) : (
            binding.mode == "sequence" ? "Sequence: " + (binding.sequence == undefined ? "None" : binding.sequence) :
            buttonText(binding.source)));
}
function buttonText(text) {
    if(!text) {
        return "--no sound--";
    }
    return text.split("/").slice(-1)[0].split(".")[0];
}

function inputChange(force) {
    if(ctx.currentTime - lastReload > 0.3) {
        soundNeedsReload = true;
        if(force || (localStorage.getItem("autoReload") != "true" && (bindings[curSettings].source != $("#sourceSelect").val() || bindings[curSettings].audio === undefined))) {
            bindings[curSettings].source = $("#sourceSelect").val();
            reloadAudio(curSettings);
        } else {
            updateReloadButton(false, true);
        }
    }
}
function reloadAudio(charNum) {
    delete bindings[charNum].audio;
    showWaveform();
    if(bindings[charNum].source.length === 0 || ["met", "alias", "sequence"].indexOf(bindings[charNum].mode) != -1) {
        setError(charNum, 0);
        return;
    }
    if(bindings[charNum].source.indexOf("db:") === 0) {
        dbReloadAudio(charNum);
        return;
    }
    if(bindings[charNum].source.indexOf("gd:") === 0) {
        gdReloadAudio(charNum);
        return;
    }
    if(bindings[charNum].source.indexOf("file:") === 0) {
        fileReloadAudio(charNum);
        return;
    }
    var scIndex = bindings[charNum].source.indexOf("://soundcloud.com/");
    if(bindings[charNum].source.indexOf("http") === 0 && scIndex < 6 && scIndex > 0) {
        scReloadAudio(charNum);
        return;
    }
    bindings[charNum].audio = undefined;
    var getSound = new XMLHttpRequest();
    getSound.open("GET", bindings[charNum].source, true);
    getSound.responseType = "arraybuffer";
    setError(charNum, 1);
    updateReloadButton(true);
    getSound.onload = function() {
        ctx.decodeAudioData(getSound.response, function(buffer) {
            if(bindings[charNum] === undefined) {
                return;
            }
            bindings[charNum].audio = buffer;
            showWaveform();
            calculateSync(bindings[charNum].audio, bindings[charNum].mode == "song");
            setError(charNum, 0);
            updateReloadButton(false);
        }, function() {
            loadError(charNum, "Sadly, your browser does not support the audio format of '" + getSound.responseURL + "'.", true);
            updateReloadButton(false);
        });
    };
    getSound.onerror = function(err) {
        loadError(charNum, "Sadly, '" + bindings[charNum].source + "' does not exist or the owner of the server it resides on has not enabled for it to be used on other websites. Try downloading it and using the sound file directly.", true);
        updateReloadButton(false);
    };
    getSound.send();
}
function nothing(event) {
    event.stopPropagation();
}
$(".frontWrapper").click(hideSettings);
$("#settings").click(nothing);
$("#sequenceEditor").click(nothing);
$("#globalSettings").click(nothing);

function loadError(charNum, message, isCompleteMessage) {
    if(isCompleteMessage === undefined) { isCompleteMessage = false; }
    if(isCompleteMessage) {
        alert(message);
    } else {
        alert("Sadly, '" + message + "' could not be loaded.");
    }
    bindings[charNum].audio = undefined;
}
function modeChange() {
    var nonLoadingModes = ["met", "alias", "sequence"];
    var prevMode = bindings[curSettings].mode;
    bindings[curSettings].mode = $("#modeSelect").val();
    if(nonLoadingModes.indexOf($("#modeSelect").val()) != -1) {
        delete bindings[curSettings].audio;
    } else {
        if(nonLoadingModes.indexOf(prevMode) != -1) {
            inputChange(false);
        }
    }
    var finishText = "Finish afterwards:";
    if($("#modeSelect").val() == "once") {
        finishText = "Repeat on multiple presses:";
    }
    var toShow = ["#finish", "#volume"];
    if($("#modeSelect").val() == "met") {
        toShow.push("#speed", "#beats");
        finishText = "Allow sync only on major beats:";
    } else if($("#modeSelect").val() == "alias") {
        toShow = ["#aliasFor"];
    } else if($("#modeSelect").val() == "sequence") {
        toShow = ["#finish", "#sequenceSelect"];
    } else {
        if($("#modeSelect").val() == "sync") {
            toShow.push("#displacement");
        }
        toShow.push("#source", "#waveformRow", "#start", "#pitch", "#decay");
    }
    $(".buttonConfig").hide();
    toShow.forEach(function(identifier) {
        $(identifier).show();
    });
    $("#finishAfterUp").html(finishText);
}
function selectAliasTarget() {
    $(".frontWrapper").hide();
    editButton(curSettings, "Cancel");
    mode = "aliasTarget";
    hideUtilButtons();
}
function setAliasTarget(charCode) {
    showUtilButtons();
    $("#settingsWrapper").show();
    mode = "settings";
    if(charCode == curSettings) {
        return;
    }
    bindings[curSettings].forAlias = parseInt(charCode);
    $("#aliasForButton > span").html(getButtonName(charCode));
}
function removeBinding() {
    if(window.confirm("Do you really want to remove the key binding?")) {
        hideSettings(undefined, 1);
        delete bindings[curSettings];
        for(var key in bindings) {
            if(bindings[key].forAlias == curSettings) {
                delete bindings[key].forAlias;
                $("#btn" + key + " > span").html("Alias: None<br>" + getButtonName(key));
            }
        }
        if($("#btn" + curSettings).length > 0) {
            $("#btn" + curSettings).remove();
        } else {
            removePianoButton(curSettings);
        }
        saveBindings();
    }
}

function hideUtilButtons() {
    $("td.header").hide();
    $("div.leftFooter").hide();
    $("div.rightFooter").hide();
    $("td#headerCancel").show();
}

function showUtilButtons() {
    $("td.header").show();
    $("div.leftFooter").show();
    $("div.rightFooter").show();
    $("td#headerCancel").hide();
}

function getLayout() {
    var buttons = $('[id^=btn]');
    return JSON.stringify([buttons.map(function() {
        var id = $(this).prop("id");
        var css = $(this).css("left");
        var obj = {};
        obj.key = id;
        obj.val = css;
        return obj;
    }).get(), buttons.map(function() {
        var id = $(this).prop("id");
        var css = $(this).css("top");
        var obj = {};
        obj.key = id;
        obj.val = css;
        return obj;
    }).get()]);
}
function saveLayout() {
    localStorage.setItem("layout", getLayout());
}
function getCookieLayout() {
    var layoutStr = localStorage.getItem("layout");
    if(!layoutStr) return [[], []];
    return JSON.parse(layoutStr);
}
function loadLayout() {
    var lo = getCookieLayout();
    for(var left in lo[0]) {
        $("#" + lo[0][left].key).css("left", lo[0][left].val);
    }
    for(var top in lo[1]) {
        $("#" + lo[1][top].key).css("top", lo[1][top].val);
    }
}
function exportLayout() {
    var shortenedBindings = $.extend({}, bindings);
    for(var key in shortenedBindings) {
        shortenedBindings[key].audio = undefined;
    }
    $("#expLo").html(btoa("[" + localStorage.getItem("bindings") + "," + getLayout() + "," + localStorage.getItem("sequences") + "]"));
    $("#exportDialog").dialog();
}
function importLayout() {
    $("#importText").val("");
    $("#importHeader").html("Enter your string below:");
    $("#importDialog").dialog({
        buttons: {
            "OK": function() {
                try {
                    var lo = $("#importText").val();
                    var loObj = JSON.parse(atob(lo));
                    $('[id^=btn]').remove();
                    bindings = loObj[0];
                    sequences = loObj[2] || {};
                    loadButtons();
                    for(var left in loObj[1][0]) {
                        $("#" + loObj[1][0][left].key).css("left", loObj[1][0][left].val);
                    }
                    for(var top in loObj[1][1]) {
                        $("#" + loObj[1][1][top].key).css("top", loObj[1][1][top].val);
                    }
                    $(this).dialog("close");
                } catch(e) {
                    $("#importHeader").html("Please enter a valid string");
                }
            },
            "Cancel": function(){
                $(this).dialog("close");
            }
        }
    });
}
function saveBindings() {
    var shortenedBindings = {};
    for(var key in bindings) {
        if(bindings[key] === undefined) {
            continue;
        }
        shortenedBindings[key] = {};
        shortenedBindings[key].mode = bindings[key].mode;
        shortenedBindings[key].finish = bindings[key].finish;
        shortenedBindings[key].source = bindings[key].source;
        shortenedBindings[key].start = bindings[key].start;
        shortenedBindings[key].stop = bindings[key].stop;
        shortenedBindings[key].volume = bindings[key].volume;
        shortenedBindings[key].deviation = bindings[key].deviation;
        shortenedBindings[key].pitch = bindings[key].pitch;
        shortenedBindings[key].bpm = bindings[key].bpm;
        shortenedBindings[key].beats = bindings[key].beats;
        shortenedBindings[key].decay = bindings[key].decay;
        shortenedBindings[key].sequence = bindings[key].sequence;
        shortenedBindings[key].forAlias = bindings[key].forAlias;
    }
    var obj = JSON.stringify(shortenedBindings);
    localStorage.setItem("bindings", obj);
    saveLayout();
}
function loadBindings() {
    var bindingsStr = localStorage.getItem("bindings");
    if(!bindingsStr) return;
    bindings = JSON.parse(bindingsStr);
    loadButtons();
}
function loadButtons() {
    if(localStorage.getItem("pianoMode") == "true") {
        $("div.mainKey").show();
    }
    for (var key in bindings) {
        if(bindings.hasOwnProperty(key)) {
            var binding = bindings[key];
            sounds[key] = [];
            createButton(key, buttonTextFromBinding(binding));
            if(binding.source) {
                reloadAudio(key);
            }
        }
    }
}
function deleteAllButtons() {
    if(window.confirm("Do you really want to remove all bindings?")) {
        bindings = {};
        saveBindings();
        $('[id^=btn]').remove();
        saveLayout();
    }
}
$("body").ready(function() {
    $("button").button().css({ width: '100%' });
    $("#record").css({ width: "auto" });
    $("#recordMicButton").css({ width: "auto" });
    $("#refreshMidi").css({ width: "auto" });
    $("#refreshSource").css({ width: "auto" });

    $("#record").button({
        icons: {primary: null},
        text: false
    });

    loadBindings();
    loadLayout();
    $("body").delay(0.5).fadeIn(1000);
});
window.onerror = undefined;
