var waveformCanvas;
var waveformSteps = 1000;
var maxArgLen = 65536;

function showWaveform() {
    if(mode != "settings") {
        return;
    }
    var width = $("#waveform")[0].width;
    var height = $("#waveform")[0].height;
    waveformCanvas.clearRect(0, 0, width, height);
    waveformCanvas.beginPath();
    waveformCanvas.moveTo(0, height / 2);
    if(bindings[curSettings].audio !== undefined) {
        var audio = bindings[curSettings].audio;
        var data = audio.getChannelData(0);
        var absMax = Math.max(arrMax(data), -arrMin(data));
        var stepSize = data.length / waveformSteps;
        for(var i = 0; i < waveformSteps; i++) {
            var point = data.subarray(i * stepSize, (i + 1) * stepSize);
            var max = arrMax(point) / absMax;
            var min = arrMin(point) / absMax;
            waveformCanvas.lineTo(width / waveformSteps * (i + 0.5), (1 - max) * height / 2);
            waveformCanvas.lineTo(width / waveformSteps * (i + 1), (1 - min) * height / 2);
        }
    } else {
        waveformCanvas.lineTo(width, height / 2);
    }
    waveformCanvas.stroke();
}

function arrMax(arr) {
    return arrExtreme(arr, Math.max);
}
function arrMin(arr) {
    return arrExtreme(arr, Math.min);
}
function arrExtreme(arr, extremeFunc) {
    var extremes = [];
    for(var i = 0; i < arr.length / maxArgLen; i++) {
        var current = arr.slice(i * maxArgLen, (i+1) * maxArgLen);
        extremes.push(extremeFunc.apply(null, current));
    }
    if(extremes.length == 1) {
        return extremes[0];
    } else {
        return arrExtreme(extremes, extremeFunc);
    }
}

$("body").ready(function() {
    waveformCanvas = $("#waveform")[0].getContext("2d");
    $("#waveform")[0].width = 1100;
    $("#waveform")[0].height = 150;
});
