# Leekboard changelog

The latest version of this file can be found at the master branch of the leekboard repository.

1.0.1

- Decay time
- Pitch slider

1.0.0

- Added metronome

0.9.0

- Made mobile-friendly
